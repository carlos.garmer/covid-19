import { Component } from '@angular/core';
import {SrvcovidService} from '../srvcovid.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
 newCountries = [];
  constructor(
    public service : SrvcovidService,
    public route : Router,
    public alertController: AlertController
  ) {}
  
  ionViewWillEnter(){
    this.service.loadSelectedCountries();
    this.service.getSummary()
    .subscribe(
      (data)=> {
        this.newCountries = this.getNewData(data['Countries'], this.service.getSelectedCountries());
        //console.log(data['Countries']);
        //console.log(this.service.getSelectedCountries());
        console.log("Paises a mostrar")
        console.log(this.newCountries);
      },
      (error)=>{
        console.error(error);
      }
    )
  }
    getNewData(countries, selected){
      let newData = [];
      for (let i = 0;  i < selected.length; i++){
        for(let j = 0; j < countries.length; j++){
        if(selected[i]['ISO2'] == countries[j]['CountryCode']){
          newData.push(countries[j]);
          break;
        }
      }
    }
      return newData;
    }
    async presentAlertConfirm(code) {
      const alert = await this.alertController.create({
        header: '¿Seguro que quieres eliminar este país?',
        message: '',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Okay',
            handler: () => {
              console.log('Confirm Okay');
              this.eliminar(code);
            }
          }
        ]
      });
  
      await alert.present();
    }

    eliminar(code){
      console.log(code);
      let temp = [];
      let selected =[];
      selected = this.service.getSelectedCountries();
      for(let j = 0; j < selected.length; j++){
        if(code != selected[j]['ISO2']){
          temp.push(selected[j]);
        }
    }
    console.log("Temporal");
    console.log(temp);
    this.service.setSelectedCountries(temp);
    this.route.navigate(['']);
}
}
