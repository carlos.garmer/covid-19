import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NpaisPage } from './npais.page';

const routes: Routes = [
  {
    path: '',
    component: NpaisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NpaisPageRoutingModule {}
