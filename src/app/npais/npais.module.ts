import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NpaisPageRoutingModule } from './npais-routing.module';

import { NpaisPage } from './npais.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NpaisPageRoutingModule
  ],
  declarations: [NpaisPage]
})
export class NpaisPageModule {}
