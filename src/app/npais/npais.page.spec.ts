import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NpaisPage } from './npais.page';

describe('NpaisPage', () => {
  let component: NpaisPage;
  let fixture: ComponentFixture<NpaisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NpaisPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NpaisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
